import SimpleOpenNI.*;

SimpleOpenNI kinect;
PVector projectiveDistance1 = new PVector(0,0,0);

PVector realCurrent;
PVector projectiveCurrent = new PVector(0,0,0);

void setup(){
  size(640, 480);
  
  kinect = new SimpleOpenNI(this);
  
  kinect.enableRGB();
  kinect.enableDepth();
  
  kinect.alternativeViewPointDepthToImage();
  
  kinect.setMirror(true);
}

PVector middle = new PVector(0, 0, 0);

float xChord;
float yChord;

float prevXAngle;
float prevYAngle;

void draw(){
  kinect.update();
  background(0);
  image(kinect.rgbImage(), 0, 0);
  
  PVector[] depthPoints = kinect.depthMapRealWorld();
  realCurrent = depthPoints[mouseX + (mouseY * 640)];
  kinect.convertRealWorldToProjective(realCurrent, projectiveCurrent);
  
  xChord = 2 * realCurrent.z * sin(kinect.hFieldOfView()/2);
  yChord = 2 * realCurrent.z * sin(kinect.vFieldOfView()/2);
  
  PVector position = new PVector(xChord/2, yChord/2, 0);
  
  float mChordX = map(mouseX, 0, 640, 0, xChord);
  float hipChordX = sqrt(pow((mChordX - position.x), 2) + pow((realCurrent.z - position.z), 2));
  float xAngle = degrees(asin(realCurrent.z / hipChordX));
  
  float mChordY = map(mouseY, 0, 480, 0, yChord);
  float hipChordY = sqrt(pow((mChordY - position.y), 2) + pow((realCurrent.z - position.z), 2));
  float yAngle = degrees(asin(realCurrent.z / hipChordY));
  
  if(Float.isNaN(xAngle)){
    xAngle = prevXAngle;
  }
  
  if(mChordX > xChord/2){
    xAngle = 180 - xAngle;
  }
  
  if(Float.isNaN(yAngle)){
    yAngle = prevYAngle;
  }
  
  if(mChordY < yChord/2){
    yAngle = 180 - yAngle;
  }
  
  stroke(0, 102, 153);
  strokeWeight(2);
  line(0, mouseY, 640, mouseY);
  
  textSize(16);
  //fill(0, 102, 153);
  text("Y:" + yAngle + "°", mouseX, mouseY - 18);
  text("X:" + xAngle + "°", mouseX, mouseY - 2);
  
  prevXAngle = xAngle;
  prevYAngle = yAngle;
}
